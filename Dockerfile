
FROM  python:3.7


COPY ./ /code

WORKDIR /code/src

RUN pip3 install -r ../requirements.txt

RUN pip3 install gym[atari]

ENTRYPOINT  jupyter notebook --ip=0.0.0.0 --port=8080 --allow-root