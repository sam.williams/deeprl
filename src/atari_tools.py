import tensorflow as tf
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def plot_state(state):
    """ plot current state environment """

    plt.figure(figsize=(12,8))
    plt.imshow(np.squeeze(state))
    plt.axis("off")
    plt.show()


def process_states(states):
    """ reduce dimensionality and resize before processing """
    
    states_array = []
    for state in states:
        state = tf.math.reduce_max(state, -1, keepdims=True)
        state = state / 255
        state = tf.image.resize(state, (84,84))
        states_array.append(state)

    stacked_array = np.stack(states_array, axis=2)
    stacked_array = np.squeeze(stacked_array)
    stacked_array = np.expand_dims(stacked_array,0) 
    
    return stacked_array



def Q_matrix(episode_state_frames, episode_actions, episode_rewards, model):

    prediction = model(episode_state_frames)
    prediction = np.vstack((prediction,np.array([0,0,0,0])))

    matrix = np.zeros(shape=(len(episode_actions),4))
    matrix[np.arange(0,len(episode_actions),1), episode_actions] = episode_rewards

    Q = np.zeros(shape=(len(episode_actions),4))
    Q[np.arange(0,len(episode_actions),1), episode_actions] = np.max(prediction[1:, :], axis=1)
    y = matrix + model.gamma*Q
    prediction = prediction[:-1]
    prediction[np.arange(0, y.shape[0], 1),np.argmax(y,1)] = np.max(y,1)
    return prediction