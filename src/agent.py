import tensorflow as tf
from tensorflow.keras.layers import Conv2D, Dense, Flatten
from atari_tools import process_states, plot_state
import numpy as np

class AtariAgent(tf.keras.Model):

    number_of_games = 0

    def __init__(self, env):

        super().__init__()
        self.gamma = 0.9
        self.action_space = env.action_space
        self.conv_layer_1 = Conv2D(16, (8,8), strides=4, activation="relu")
        self.conv_layer_2 = Conv2D(32, (4,4), strides=2, activation="relu")
        self.flatten = Flatten()
        self.dense = Dense(256, activation="relu")
        self.prediction = Dense(4)


    @property
    def epsilon(self):
        return max(AtariAgent.number_of_games*(0.1-1/10000) + 1, 0.1)

    
    def call(self, states):

        x = self.conv_layer_1(states)
        x = self.conv_layer_2(x)
        x = self.flatten(x)
        x = self.dense(x)
        prediction = self.prediction(x)
        return prediction

